<?php

namespace Application\Lib;

use PDO;
use PDOException;

class Connect {

    private static $conn;

    private function __construct ()
    {

    }

    private function __clone ()
    {

    }

    private function __wakeup ()
    {

    }

    public static function connectionWithPDO()
    {
        try {
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_STRINGIFY_FETCHES => false,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ];
            if (!self::$conn) {
                self::$conn = new PDO("mysql:host=mysql;dbname=graphics_editor;charset=utf8", "root", "rootroot", $options);
            }
            return self::$conn;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

}

