<?php

namespace Application\Contracts;

interface Color
{
    public function detectColor();
}