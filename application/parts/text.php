<form method="POST" action="/draw">
<input id="type-of-figure" name="type-of-figure" type="text" class="hidden" value="text">
<h3 class="figure-right">Произвольный текст</h3>
<h4 class="right-field">Координаты левой нижней точки:</h4>
<div class="input-group mb-3 flexible">
    <span class="from-pair">
    <input id="x1" name="x1" type="text" class="form-control" placeholder="по оси X">
    </span>
    <span class="from-pair">
    <input id="y1" name="y1" type="text" class="form-control" placeholder="по оси Y">
    </span>
</div>
<div class="input-group mb-3 flexible">
    <span class="from-pair">
    <h4 class="right-field text">Размер шрифта:</h4>
    <input id="font-size" name="font-size" type="text" class="form-control" placeholder="">
    </span>
    <span class="from-pair">
    <h4 class="right-field text">Угол наклона:</h4>
    <input id="text-angle" name="text-angle" type="text" class="form-control" placeholder="(против часовой)">
    </span>
</div>
<h4 class="right-field">Поле для ввода текста:</h4>
<div class="form-floating">
  <textarea id="mytextarea" name="text" type="text" class="form-control text"></textarea>
</div>
<h4 class="right-field">Выберите цвет текста:</h4>
<div class="input-group mb-3 flexible">
<select id="color" name="color" class="form-select form-select-sm color">
  <option value="red">Красный</option>
  <option value="orange">Оранжевый</option>
  <option value="yellow">Желтый</option>
  <option value="green">Зеленый</option>
  <option value="lightblue">Голубой</option>
  <option value="blue">Синий</option>
  <option value="violet">Фиолетовый</option>
  <option value="white">Белый</option>
  <option value="black">Черный</option>
</select>
<button id="submit-figure" type="button" class="btn btn-primary right-field">Отправить</button>
</div>
</form>

<form method="POST" action="/save">
    <div class="d-grid mx-auto">
        <input name="saving-image" type="text" class="hidden" value="yes">
        <button class="btn btn-primary savechanges" type="submit">Сохранить изменения</button>
    </div>
</form>