<form method="POST" action="/draw">
<input id="type-of-figure" name="type-of-figure" type="text" class="hidden" value="rectangle">
<h3 class="figure-right">Прямоугольник</h3>
<h4 class="right-field">Координаты одной из вершин:</h4>
<div class="input-group mb-3 flexible">
    <span class="from-pair">
    <input id="x1" name="x1" type="text" class="form-control" placeholder="по оси X">
    </span>
    <span class="from-pair">
    <input id="y1" name="y1" type="text" class="form-control" placeholder="по оси Y">
    </span>
</div>
<h4 class="right-field">Координаты противоположной вершины:</h4>
<div class="input-group mb-3 flexible">
    <span class="from-pair">
    <input id="x2" name="x2" type="text" class="form-control" placeholder="по оси X">
    </span>
    <span class="from-pair">
    <input id="y2" name="y2" type="text" class="form-control" placeholder="по оси Y">
    </span>
</div>
<h4 class="right-field">Выберите цвет контура:</h4>
<div class="input-group mb-3 flexible">
<select id="color" name="color" class="form-select form-select-sm color">
  <option value="red">Красный</option>
  <option value="orange">Оранжевый</option>
  <option value="yellow">Желтый</option>
  <option value="green">Зеленый</option>
  <option value="lightblue">Голубой</option>
  <option value="blue">Синий</option>
  <option value="violet">Фиолетовый</option>
  <option value="white">Белый</option>
  <option value="black">Черный</option>
</select>
<button id="submit-figure" type="button" class="btn btn-primary right-field">Отправить</button>
</div>
</form>

<form method="POST" action="/save">
    <div class="d-grid mx-auto">
        <input name="saving-image" type="text" class="hidden" value="yes">
        <button class="btn btn-primary savechanges" type="submit">Сохранить изменения</button>
    </div>
</form>