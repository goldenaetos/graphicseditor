<?php

namespace Application\Services;

class UploadImage {

    protected const UPLOADFILEDIR = './uploaded_files/';

    protected $dest_path;

    public function setImage(string $fileTmpPath, string $fileName, string $fileExtension): void
    {
        $this->fileTmpPath = $fileTmpPath;
        $this->fileName = $fileName;
        $this->fileExtension = $fileExtension;
        $this->newFileName = md5(time() . $this->fileName) . '.' . $this->fileExtension;
        $this->dest_path = self::UPLOADFILEDIR . $this->newFileName;
        move_uploaded_file($this->fileTmpPath, $this->dest_path);
        $_SESSION['path-to-image'] = $this->dest_path;
    }

    public function getDestPath(): string
    {
        return $this->dest_path;
    }

    public function setDestPath(string $dest_path): void
    {
        $this->dest_path = $dest_path;
    }
}