<?php

namespace Application\Services;

use Application\Models\User;
use Application\Models\Image;
use Exception;

class ImageService {

    public function validation(string $fileExtension): void
    {
        if (($fileExtension !== 'jpg') && ($fileExtension !== 'jpeg') && ($fileExtension !== 'png')) {
            throw new Exception('Неправильный формат файла! Загрузите картинку с расширением jpg, jpeg или png.');
        }
    }

    public function uploadImage(array $file): string
    {
        $fileNameParts = explode(".", $file['name']);
        $fileExtension = strtolower(end($fileNameParts));
        $this->validation($fileExtension);
        $fileTmpPath = $file['tmp_name'];
        $fileName = $file['name'];
        $image = new UploadImage();
        $image->setImage($fileTmpPath, $fileName, $fileExtension);
        unset($_SESSION['messageAboutSaving']);
        return $image->getDestPath();
    }

    public function drawFigure(array $figureProperties, string $pathToImage)
    {
        $partsOfPathToImage = explode('/', $pathToImage);
        if ($partsOfPathToImage[0] === 'img_result') {
            $newPathToImage = 'uploaded_files/' . end($partsOfPathToImage);
            copy($pathToImage, $newPathToImage);
            $_SESSION['path-to-image'] = $pathToImage = $newPathToImage;
        }
        $colorFactory = new ColorFactory();
        $colorOfFigure = $colorFactory->create($figureProperties['color']);
        $colorOfFigure->detectColor();
        $colorDigits = $colorOfFigure->colorArray;
        $figureFactory = new FigureFactory();
        $figure = $figureFactory->create($colorDigits, $figureProperties);
        if ($pathToImage) {
            $extension = strtolower(end(explode('.', $pathToImage)));
            $extension === 'png' ? $figure->drawFromPng($colorDigits, $pathToImage)
                : $figure->drawFromJpg($colorDigits, $pathToImage);
        }
        echo $pathToImage;
        return $pathToImage;
    }

    public function savingImage(string $pathToImage, string $userEmail): void
    {
        if (file_exists($pathToImage))
        {
            if (strtolower(end(explode('.', $pathToImage))) === 'png') {
                $imagecreatefromSomething = 'imagecreatefrompng';
                $imageFromSomething = 'imagepng';
            } else {
                $imagecreatefromSomething = 'imagecreatefromjpeg';
                $imageFromSomething = 'imagejpeg';
            }
            $image = $imagecreatefromSomething($pathToImage);
            $imageSavingPathAndName = 'img_result/image_' . date('Y_m_d_H_i_s') . '.jpg';
            $imageFromSomething($image, $imageSavingPathAndName);
            $sketchSavingPathAndName = 'img_sketch/sketch_' . date('Y_m_d_H_i_s') . '.jpg';
            $image = imagescale($image, 100);
            $imageFromSomething($image, $sketchSavingPathAndName);
            $_SESSION['path-to-sketch'] = $sketchSavingPathAndName;
            $_SESSION['path-to-image'] = $imageSavingPathAndName;
            imagedestroy($image);
            unlink($pathToImage);
            $userID = (new User)->read($userEmail)['id'];
            $imageParameters = [
                'user_id' => $userID,
                'user_email' => $userEmail,
                'image_path' => $imageSavingPathAndName,
                'sketch_path' => $sketchSavingPathAndName
            ];
            (new Image)->savingInDatabase($imageParameters);
            $_SESSION['messageAboutSaving'] = 'Изображение успешно сохранено.';
            unset(
                $_SESSION['path-to-image'],
                $_SESSION['path-to-sketch']
            );
        }
   }

}