<?php

namespace Application\Services;

use Application\Models\Figures\Circle;
use Application\Models\Figures\Linesegment;
use Application\Models\Figures\Rectangle;
use Application\Models\Figures\Square;
use Application\Models\Figures\Text;
use Application\Models\Figures\Triangle;

class FigureFactory
{

    public function create(array $colorDigits, array $figureProperties): object
    {
        switch ($figureProperties['type-of-figure']) {
            case 'square':
                return new Square($colorDigits, $figureProperties);
                break;

            case 'rectangle':
                return new Rectangle($colorDigits, $figureProperties);
                break;

            case 'circle':
                return new Circle($colorDigits, $figureProperties);
                break;

            case 'linesegment':
                return new Linesegment($colorDigits, $figureProperties);
                break;

            case 'triangle':
                return new Triangle($colorDigits, $figureProperties);
                break;
                
            default:
                return new Text($colorDigits, $figureProperties);
                break;
        }

    }
    
}