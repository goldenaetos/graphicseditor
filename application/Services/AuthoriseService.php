<?php

namespace Application\Services;

use Application\Models\User;
use Exception;

class AuthoriseService
{
    private $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function register(array $newUserData): void
    {
        try {
            $user = $this->user->read($newUserData['email']);
            if (!$user) {
                $this->user->create($newUserData);
                $_SESSION['user_email'] = $newUserData['email'];
                header('location: /');
            } else {
                throw new Exception ("<b>Данный email уже используется в системе!<br>Если Вы зарегистрированы, для авторизации нажмите <a href='/account/login'>Войти</a>.</b>");
            }
        } catch (Exception $error) {
            echo $error->getMessage();
        }
     }

    public function auth(array $loginUserData): void
    {
        try {
            $user = $this->user->read($loginUserData['email'], $loginUserData['password']);
            if (!empty($user)) {
                $_SESSION['user_email'] = $user['email'];
                header('location: /');
            } else {
                throw new Exception("<b>Введите корректные данные!<br>Если Вы не зарегистрированы в системе, милости просим к нам: <a href='/account/register'>Регистрация</a>.</b>");
            }
        } catch (Exception $error) {
            echo $error->getMessage();
        }
    }

    public function logout(): void
    {
        $_SESSION = [];
        session_destroy();
        setcookie("PHPSESSID", "", time() - 86400, '/');
        header('location: /account/login');
    }
}