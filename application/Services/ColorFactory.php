<?php

namespace Application\Services;

use Application\Models\Colors\Black;
use Application\Models\Colors\Blue;
use Application\Models\Colors\Lightblue;
use Application\Models\Colors\Green;
use Application\Models\Colors\Orange;
use Application\Models\Colors\Red;
use Application\Models\Colors\Violet;
use Application\Models\Colors\White;
use Application\Models\Colors\Yellow;

class ColorFactory
{

    public function create(string $color): object
    {
        switch ($color) {
            case 'red':
                return new Red();
                break;

            case 'orange':
                return new Orange();
                break;

            case 'yellow':
                return new Yellow();
                break;

            case 'green':
                return new Green();
                break;

            case 'lightblue':
                return new Lightblue();
                break;

            case 'blue':
                return new Blue();
                break;

            case 'violet':
                return new Violet();
                break;

            case 'white':
                return new White();
                break;
    
            default:
                return new Black();
                break;
        }

    }
    
}