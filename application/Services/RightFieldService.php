<?php

namespace Application\Services;

class RightFieldService
{
    public function showRightField($figure)
    {
        switch ($figure) {
            case 'square':
                $requestResult = 'application/parts/square.php';
                break;

            case 'rectangle':
                $requestResult = 'application/parts/rectangle.php';
                break;

            case 'circle':
                $requestResult = 'application/parts/circle.php';
                break;

            case 'linesegment':
                $requestResult = 'application/parts/linesegment.php';
                break;

            case 'triangle':
                $requestResult = 'application/parts/triangle.php';
                break;

            default:
                $requestResult = 'application/parts/text.php';
                break;
        }
        return $requestResult;
    }
}