<?php
if(!$_SESSION['user_email'] || $_SESSION['user_email'] == null)
{
    header('location: /account/login');
}
?>

<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>Charts | Porto Admin - Responsive HTML5 Template</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="/vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="/vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="/vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="/vendor/morris/morris.css" />
    <link rel="stylesheet" href="/vendor/chartist/chartist.min.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="/css/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="/css/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="/css/custom.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="/css/main.css" />

    <!-- Head Libs -->
    <script src="/vendor/modernizr/modernizr.js"></script>

</head>
<body>
<section class="body">

    <!-- start: header -->
    <header class="header">
        <div class="logo-container">
            <a href="#" class="logo">
                <img src="/img/logo.png" width="75" height="35" alt="Porto Admin" />
            </a>
        </div>

        <!-- start: user box -->
        <div class="header-right">
            <span class="separator"></span>
            <div id="userbox" class="userbox">
                <a href="#" data-bs-toggle="dropdown">
                    <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                        <span class="name"><?=$_SESSION['user_email']; ?></span>
                    </div>
                    <i class="fa custom-caret"></i>
                </a>
                <div class="dropdown-menu">
                    <ul class="list-unstyled mb-2">
                        <li class="divider"></li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="/images/list"><i class="bx bx-user-circle"></i> My Images</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="/account/logout"><i class="bx bx-power-off"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end: user box -->
    </header>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
                    Выберите фигуру
                </div>
                <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">
                        <ul class="nav nav-main">
                            <li class="square">
                                <a class="nav-link" href="#">
                                    <img src="/img/icons/square.png">
                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;Квадрат</span>
                                </a>
                            </li>
                            <li class="rectangle">
                                <a class="nav-link" href="#">
                                    <img src="/img/icons/rectangle.png">
                                    <span>&nbsp;&nbsp;&nbsp;Прямоугольник</span>
                                </a>
                            </li>
                            <li class="circle">
                                <a class="nav-link" href="#">
                                    <i class="bx bx-circle" aria-hidden="true"></i>
                                    <span>Окружность</span>
                                </a>
                            </li>
                            <li class="linesegment">
                                <a class="nav-link" href="#">
                                    <img src="/img/icons/linesegment.png">
                                    <span>&nbsp;&nbsp;&nbsp;Отрезок</span>
                                </a>
                            </li>
                            <li class="triangle">
                                <a class="nav-link" href="#">
                                    <img src="/img/icons/triangle.png">
                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;Треугольник</span>
                                </a>
                            </li>
                            <li class="text">
                                <a class="nav-link" href="#">
                                    <img src="/img/icons/text.png">
                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;Текст</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <script>
                    // Maintain Scroll Position
                    if (typeof localStorage !== 'undefined') {
                        if (localStorage.getItem('sidebar-left-position') !== null) {
                            var initialPosition = localStorage.getItem('sidebar-left-position'),
                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                            sidebarLeft.scrollTop = initialPosition;
                        }
                    }
                </script>

            </div>
        </aside>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <a href="/"><h2>Графический редактор</h2></a>
            </header>

            <!-- start: page -->
            <h4 class="mt-0 mb-0 font-weight-bold text-dark" style="padding-bottom: 10px;">Загруженные изображения</h4>

            <div class="row">
                <div class="col-lg-12">
                    <section class="card">
                        <div class="card-body">
                            <div id="my-images">

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Sketch</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Date</th>
                                    </tr>
                                    </thead>
                                    <tbody class="table-with-sketches" >
                                        <?php
                                        foreach ($images as $numberOfRow => $infoAboutImage) {
                                        ?>
                                            <tr data-pathtosketch="<?= $infoAboutImage['sketch']; ?>">
                                                <th scope="row"><?=$numberOfRow + 1; ?></th>
                                                <td><img src="/<?= $infoAboutImage['sketch']; ?>"></td>
                                                <td><?=$infoAboutImage['username']; ?></td>
                                                <td><?=$infoAboutImage['date']; ?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <!-- end: page -->
        </section>
    </div>

</section>

<!-- Vendor -->
<script src="/vendor/jquery/jquery.js"></script>
<script src="/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="/vendor/popper/umd/popper.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/vendor/common/common.js"></script>
<script src="/vendor/nanoscroller/nanoscroller.js"></script>
<script src="/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="/vendor/jquery-appear/jquery.appear.js"></script>
<script src="/vendor/jquery.easy-pie-chart/jquery.easypiechart.js"></script>
<script src="/vendor/flot/jquery.flot.js"></script>
<script src="/vendor/flot.tooltip/jquery.flot.tooltip.js"></script>
<script src="/vendor/flot/jquery.flot.pie.js"></script>
<script src="/vendor/flot/jquery.flot.categories.js"></script>
<script src="/vendor/flot/jquery.flot.resize.js"></script>
<script src="/vendor/jquery-sparkline/jquery.sparkline.js"></script>
<script src="/vendor/raphael/raphael.js"></script>
<script src="/vendor/morris/morris.js"></script>
<script src="/vendor/gauge/gauge.js"></script>
<script src="/vendor/snap.svg/snap.svg.js"></script>
<script src="/vendor/liquid-meter/liquid.meter.js"></script>
<script src="/vendor/chartist/chartist.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/js/theme.js"></script>

<!-- Theme Custom -->
<script src="/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/js/theme.init.js"></script>

<!-- My Scrtipt -->
<script src="/js/main.js"></script>
</body>
</html>