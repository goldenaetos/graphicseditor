<?php

namespace Application\Controllers;

use Application\Core\Controller;
use Application\Services\ImageService;
use Application\Models\Image;

use Application\Services\RightFieldService;
use Exception;

class MainController extends Controller
{

    public function indexAction(): void
    {
        if ($_GET['email']) {
            $image = new Image();
            $result = $image->read($_GET['email'], $_GET['img']);
            $_SESSION['path-to-image'] = $result['image'];
        }
        $this->view->render('Главная страница');
    }

    public function rightfieldAction(): void
    {
        if ($_GET['figure']) {
            $rightFieldService = new RightFieldService();
            $requestResult = $rightFieldService->showRightField($_GET['figure']);
            $_SESSION['requestPath'] = $requestResult;
            header('location: /');
        }
    }

    public function uploadAction(): void
    {
        if (!empty($_FILES) && ($_POST['uploadBtn'] === 'Загрузить')) {
            $imageService = new ImageService();
            try {
                $result = $imageService->uploadImage($_FILES['uploadedFile']);
                $_SESSION['path-to-image'] = $result;
            } catch(Exception $error) {
                $images = ['errors' => $error->getMessage()];
            }
            header('Location: /');
        }
    }

    public function drawAction(): void
    {
        if (!empty($_POST['type-of-figure'])) {
            $imageService = new ImageService();
            $result = $imageService->drawFigure($_POST, $_SESSION['path-to-image']);
            $_SESSION['path-to-image'] = $result;
        }
    }

    public function saveAction(): void
    {
        if ($_POST['saving-image'] == 'yes') {
            $imageService = new ImageService();
            $imageService->savingImage($_SESSION['path-to-image'], $_SESSION['user_email']);
            header('location: /');
        }
    }

}