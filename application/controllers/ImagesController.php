<?php

namespace Application\Controllers;

use Application\Core\Controller;
use Application\Models\ListOfImagesModel;
use Exception;

class ImagesController extends Controller
{
    public function listAction(): void
    {
        $listOfImagesModel = new ListOfImagesModel();
        $arrayOfImages = $listOfImagesModel->showImages();
        $this->view->render('Загруженные изображения', $arrayOfImages);
    }
}