<?php

namespace Application\Controllers;

use Application\Core\Controller;
use Application\Services\AuthoriseService;
use Exception;

class AccountController extends Controller
{

    public function loginAction(): void
    {
        $authoriseService = new AuthoriseService();
        if(!empty($_POST['login'])) {
            $authoriseService->auth($_POST);
        }
        $this->view->render('Вход');
    }

    public function registerAction (): void
    {
        $authoriseModel = new AuthoriseService();
        if(!empty($_POST['registration'])) {
            $authoriseModel->register($_POST);
        }
        $this->view->render('Регистрация');
    }

    public function logoutAction (): void
    {
        $authoriseService = new AuthoriseService();
        try {
            $authoriseService->logout();
        } catch (Exception $error) {
            echo $error->getMessage();
        }
    }

}