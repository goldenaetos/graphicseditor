<?php

namespace Application\Models;
use Exception;

abstract class Figure {

    protected const IMAGE_THICKNESS = 5;

    public abstract function createFigure(): void;

    public function drawFromJpg(array $colorDigits, string $pathToImage): void
    {
        if ($pathToImage) {
            $this->imagePath = $pathToImage;
            $this->image = imagecreatefromjpeg($this->imagePath);
            imageSetThickness($this->image, self::IMAGE_THICKNESS);
            $this->color = imagecolorallocate($this->image, $colorDigits[0], $colorDigits[1], $colorDigits[2]);
            $this->createFigure();
            imagejpeg($this->image, $this->imagePath);
            imagedestroy($this->image);
        }
    }

    public function drawFromPng(array $colorDigits, string $pathToImage): void
    {
        if ($pathToImage) {
            $this->imagePath = $pathToImage;
            $this->image = imagecreatefrompng($this->imagePath);
            imageSetThickness($this->image, self::IMAGE_THICKNESS);
            $this->color = imagecolorallocate($this->image, $colorDigits[0], $colorDigits[1], $colorDigits[2]);
            $this->createFigure();
            imagepng($this->image, $this->imagePath);
            imagedestroy($this->image);
        }
    }

}