<?php

namespace Application\Models;

use PDO;

class Image extends DbModel
{
    public function savingInDatabase (array $imageParameters): void
    {
        $statement = $this->pdo->prepare(
            'INSERT INTO images (user_id, user_email, image, sketch) 
                   VALUES (:user_id, :user_email, :image, :sketch)'
        );
        $statement->execute([
            'user_id' => $imageParameters['user_id'],
            'user_email' => $imageParameters['user_email'],
            'image' => $imageParameters['image_path'],
            'sketch' => $imageParameters['sketch_path']
        ]);
    }

    public function read(string $authorEmail, string $partOfImageName)
    {
        $statement = $this->pdo->prepare('SELECT * FROM images WHERE user_email = :user_email AND image LIKE :partOfImageName');
        $statement->execute([
            'user_email' => $authorEmail,
            'partOfImageName' => '%' . $partOfImageName . '%'
        ]);
        unset(
            $_SESSION['path-to-sketch'],
            $_SESSION['messageAboutSaving'],
            $_SESSION['path-to-result-image']
        );
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

 }