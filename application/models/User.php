<?php

namespace Application\Models;
use PDO;

class User extends DbModel
{
    public function create(array $newUserData): void
    {
        $statement = $this->pdo->prepare('INSERT INTO users (email, password) VALUES (:email, :password)');
        $statement->execute([
            'email' => $newUserData['email'],
            'password' => $newUserData['password']
        ]);
    }

    public function read(string $userEmail, $userPassword = null)   // array | bool
    {
        $query = 'SELECT * FROM users WHERE email = :email';
        if ($userPassword) {
            $query .= ' AND password = :password';
        }
        $statement = $this->pdo->prepare($query);
        if ($userPassword) {
            $statement->execute(['email' => $userEmail, 'password' => $userPassword]);
        } else {
            $statement->execute(['email' => $userEmail]);
        }
        return $statement->fetch(PDO::FETCH_ASSOC);
    }
 }