<?php

namespace Application\Models;

use Application\Lib\Connect;

abstract class DbModel
{
    public $pdo;

    public function __construct ()
    {
        $this->pdo = Connect::connectionWithPDO();
    }
}