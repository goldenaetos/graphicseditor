<?php

namespace Application\Models\Colors;

use Application\Contracts\Color;

class White implements Color
{
    
    const COLOR_ARRAY = [255, 255, 255];

    public function detectColor(): void
    {
        $this->colorArray = self::COLOR_ARRAY;
    }

}