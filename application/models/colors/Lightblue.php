<?php

namespace Application\Models\Colors;

use Application\Contracts\Color;

class Lightblue implements Color
{
    
    const COLOR_ARRAY = [66, 170, 255];

    public function detectColor(): void
    {
        $this->colorArray = self::COLOR_ARRAY;
    }

}