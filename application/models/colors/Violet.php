<?php

namespace Application\Models\Colors;

use Application\Contracts\Color;

class Violet implements Color
{
    
    const COLOR_ARRAY = [139, 0, 255];

    public function detectColor(): void
    {
        $this->colorArray = self::COLOR_ARRAY;
    }

}