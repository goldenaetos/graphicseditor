<?php

namespace Application\Models\Colors;

use Application\Contracts\Color;

class Orange implements Color
{
    
    const COLOR_ARRAY = [255, 165, 0];

    public function detectColor(): void
    {
        $this->colorArray = self::COLOR_ARRAY;
    }

}