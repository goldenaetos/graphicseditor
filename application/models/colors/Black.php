<?php

namespace Application\Models\Colors;

use Application\Contracts\Color;

class Black implements Color
{
    
    const COLOR_ARRAY = [0, 0, 0];

    public function detectColor(): void
    {
        $this->colorArray = self::COLOR_ARRAY;
    }

}