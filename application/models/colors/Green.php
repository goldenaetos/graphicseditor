<?php

namespace Application\Models\Colors;

use Application\Contracts\Color;

class Green implements Color
{
    
    const COLOR_ARRAY = [0, 128, 0];

    public function detectColor(): void
    {
        $this->colorArray = self::COLOR_ARRAY;
    }

}