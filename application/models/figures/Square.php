<?php

namespace Application\Models\Figures;

use Application\Models\Figure;

class Square extends Figure {

    public function __construct(array $colorDigits, array $figureProperties)
    {
        $this->x1 = $figureProperties['x1'];
        $this->y1 = $figureProperties['y1'];
        $this->x2 = $this->x1 + $figureProperties['width'];
        $this->y2 = $this->y1 + $figureProperties['width'];
    }

    public function createFigure(): void
    {
        imageRectangle($this->image, $this->x1, $this->y1, $this->x2, $this->y2, $this->color);
    }

}