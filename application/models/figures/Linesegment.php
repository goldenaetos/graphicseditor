<?php

namespace Application\Models\Figures;

use Application\Models\Figure;

class Linesegment extends Figure {

    public function __construct(array $colorDigits, array $figureProperties)
    {
        $this->x1 = $figureProperties['x1'];
        $this->y1 = $figureProperties['y1'];
        $this->x2 = $figureProperties['x2'];
        $this->y2 = $figureProperties['y2'];
    }

    public function createFigure(): void
    {
        imageLine($this->image, $this->x1, $this->y1, $this->x2, $this->y2, $this->color);
    }

}