<?php

namespace Application\Models\Figures;

use Application\Models\Figure;

class Text extends Figure {

    protected const FONT_PATH = './css/fonts/Georgia.ttf';

    public function __construct(array $colorDigits, array $figureProperties)
    {
        $this->x1 = $figureProperties['x1'];
        $this->y1 = $figureProperties['y1'];
        $this->fontSize = $figureProperties['font-size'];
        $this->textAngle = $figureProperties['text-angle'];
        $this->text = $figureProperties['text'];
    }

    public function createFigure(): void
    {
        imagettftext($this->image, $this->fontSize, $this->textAngle, $this->x1, $this->y1, $this->color, self::FONT_PATH, $this->text);
    }

}