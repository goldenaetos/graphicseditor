<?php

namespace Application\Models\Figures;

use Application\Models\Figure;

class Circle extends Figure {

    public function __construct(array $colorDigits, array $figureProperties)
    {
        $this->x1 = $figureProperties['x1'];
        $this->y1 = $figureProperties['y1'];
        $this->width = $figureProperties['width'];
    }

    public function createFigure(): void
    {
        $width1 = $height1 = $this->width;
        $width2 = $height2 = $width1 - 1;
        $width3 = $height3 = $width1 - 2;
        $width4 = $height4 = $width1 - 3;
        $width5 = $height5 = $width1 - 4;
        $width6 = $height6 = $width1 - 5;
        $width7 = $height7 = $width1 - 6;
        imageEllipse ($this->image, $this->x1, $this->y1, $width1, $height1, $this->color);
        imageEllipse ($this->image, $this->x1, $this->y1, $width2, $height2, $this->color);
        imageEllipse ($this->image, $this->x1, $this->y1, $width3, $height3, $this->color);
        imageEllipse ($this->image, $this->x1, $this->y1, $width4, $height4, $this->color);
        imageEllipse ($this->image, $this->x1, $this->y1, $width5, $height5, $this->color);
        imageEllipse ($this->image, $this->x1, $this->y1, $width6, $height6, $this->color);
        imageEllipse ($this->image, $this->x1, $this->y1, $width7, $height7, $this->color);
    }

}