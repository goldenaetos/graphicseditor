<?php

namespace Application\Models\Figures;

use Application\Models\Figure;

class Triangle extends Figure {

    public function __construct(array $colorDigits, array $figureProperties)
    {
        $this->x1 = $figureProperties['x1'];
        $this->y1 = $figureProperties['y1'];
        $this->x2 = $figureProperties['x2'];
        $this->y2 = $figureProperties['y2'];
        $this->x3 = $figureProperties['x3'];
        $this->y3 = $figureProperties['y3'];
    }

    public function createFigure(): void
    {
        $points = [$this->x1, $this->y1, $this->x2, $this->y2, $this->x3, $this->y3];
        imagePolygon($this->image, $points, 3, $this->color);
    }

}