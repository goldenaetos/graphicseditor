<?php

namespace Application\Models;

class ListOfImagesModel extends DbModel
{

    public function showImages(): array
    {
        $arrayOfImages = [];
        $statement = $this->pdo->query('SELECT * FROM images ORDER BY date_time DESC');
        foreach ($statement as $number => $value)
        {
            $arrayOfImages[$number] = [
                'username' => $value['user_email'],
                'sketch' => $value['sketch'],
                'date' => (explode(" ", $value['date_time']))[0]
            ];
        }
        return $arrayOfImages;
    }
}