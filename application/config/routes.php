<?php

return [

    '' => [
        'controller' => 'main',
        'action' => 'index'
    ],

    'upload' => [
        'controller' => 'main',
        'action' => 'upload'
    ],

    'draw' => [
        'controller' => 'main',
        'action' => 'draw'
    ],

    'save' => [
        'controller' => 'main',
        'action' => 'save'
    ],

    'rightfield' => [
        'controller' => 'main',
        'action' => 'rightfield'
    ],

    'account/login' => [
        'controller' => 'account',
        'action' => 'login'
    ],

    'account/register' => [
        'controller' => 'account',
        'action' => 'register'
    ],

    'account/logout' => [
        'controller' => 'account',
        'action' => 'logout'
    ],

    'images/list' => [
        'controller' => 'images',
        'action' => 'list'
    ]

];