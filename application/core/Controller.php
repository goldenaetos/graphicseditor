<?php

namespace Application\Core;

use Application\Core\View;

abstract class Controller {

    public $route;
    public $view;

    public function __construct(array $route)
    {
        $this->route = $route;
        $this->view = new View($route);
    }

    public function loadModel(string $name): string
    {
        $path = 'application\models\\'.ucfirst($name);
        if (class_exists($path)) {
            return new $path;
        }
    }

}
