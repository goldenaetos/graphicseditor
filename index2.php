<?php

echo 'PHP Works<br><br>';

// // Обычный mysqli:
// $connect = new mysqli('mysql', 'root', 'rootroot', 'graphics_editor');
// $result = $connect->query("SELECT * FROM images WHERE user_id = 2");
// $numberOfRows = mysqli_num_rows($result);

// echo '<br><br>';

// $i = 0;
// while ($i < $numberOfRows) {
//     $row = mysqli_fetch_assoc($result);
//     $i++;
//     echo 'эскиз пользователя: ' . $row['sketch'] . '<br><br>';
// }



// PDO:
// (коннект)

try {
    // подключаемся к серверу
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_STRINGIFY_FETCHES => false,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];
	$pdo = new PDO("mysql:host=mysql;dbname=graphics_editor;charset=utf8", "root", "rootroot", $options);
    echo "Соединение с базой данных (по PDO) установлено.<br><br>";
}
catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}


// PDO:
// (выодим данные из таблицы)

//$stmt = $pdo->query('SELECT * FROM images');
//while ($row = $stmt->fetch())
//{
//    echo $row['user_id'] . ": " . $row['image'] . ": " . $row['sketch'] . ": " . $row['date_time'] . "<br>";
//}
//echo '<br><br>или же (массив для вывода другой):<br><br>';
//foreach ($stmt as $row)
//{
//    echo $row['user_id'] . ": " . $row['image'] . ": " . $row['sketch'] . ": " . $row['date_time'] . "<br>";
//}


// PDO:
// (подготовленные выражения, именованные)
$my = 2;
$stmt = $pdo->prepare('SELECT * FROM images WHERE user_id = :user_id');
$stmt->execute(array('user_id' => $my));
foreach ($stmt as $row)
{
    echo $row['user_id'] . ": " . $row['image'] . ": " . $row['sketch'] . ": " . $row['date_time'] . "<br>";
}

//// PDO:
//// (подготовленные выражения, неименованные)
//$my = 2;
//$stmt = $pdo->prepare('SELECT * FROM images WHERE user_id = ?');
//$stmt->execute([2]);
//foreach ($stmt as $row)
//{
//    echo $row['user_id'] . ": " . $row['image'] . ": " . $row['sketch'] . ": " . $row['date_time'] . "<br>";
//}

