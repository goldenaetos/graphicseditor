let myImg = document.querySelector("#flotBasic img");
    myCustomField = document.querySelector("#flotRealTime");
    tableWithSketches = document.querySelector(".table-with-sketches");
    leftField = document.querySelector("#flotBasic");

    if (document.querySelector("#submit-figure")) {
        submitButton = document.querySelector("#submit-figure");
        typeOfFigure = document.querySelector("#type-of-figure").value;
        console.dir(submitButton);
        submitButton.onclick = function()
        {
            switch (typeOfFigure) {
                case 'circle':
                    x1 = document.querySelector("#x1").value;
                    y1 = document.querySelector("#y1").value;
                    width = document.querySelector("#width").value;
                    color = document.querySelector("#color").value;
                    data = "type-of-figure=circle&x1=" + x1 + "&y1=" + y1 + "&width=" + width + "&color=" + color;
                    ajax = new XMLHttpRequest();
                    ajax.open("POST", '/draw', false);
                    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    ajax.send(data);
                    leftField.innerHTML = "<img src=" + ajax.response + "?" + Date.now() + ">";
                    break;
                case 'rectangle':
                    x1 = document.querySelector("#x1").value;
                    y1 = document.querySelector("#y1").value;
                    x2 = document.querySelector("#x2").value;
                    y2 = document.querySelector("#y2").value;
                    color = document.querySelector("#color").value;
                    data = "type-of-figure=rectangle&x1=" + x1 + "&y1=" + y1 + "&x2=" + x2 + "&y2=" + y2 + "&color=" + color;
                    ajax = new XMLHttpRequest();
                    ajax.open("POST", '/draw', false);
                    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    ajax.send(data);
                    leftField.innerHTML = "<img src=" + ajax.response + "?" + Date.now() + ">";
                    break;
                case 'linesegment':
                    x1 = document.querySelector("#x1").value;
                    y1 = document.querySelector("#y1").value;
                    x2 = document.querySelector("#x2").value;
                    y2 = document.querySelector("#y2").value;
                    color = document.querySelector("#color").value;
                    data = "type-of-figure=linesegment&x1=" + x1 + "&y1=" + y1 + "&x2=" + x2 + "&y2=" + y2 + "&color=" + color;
                    ajax = new XMLHttpRequest();
                    ajax.open("POST", '/draw', false);
                    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    ajax.send(data);
                    leftField.innerHTML = "<img src=" + ajax.response + "?" + Date.now() + ">";
                    break;
                case 'square':
                    x1 = document.querySelector("#x1").value;
                    y1 = document.querySelector("#y1").value;
                    width = document.querySelector("#width").value;
                    color = document.querySelector("#color").value;
                    data = "type-of-figure=square&x1=" + x1 + "&y1=" + y1 + "&width=" + width + "&color=" + color;
                    ajax = new XMLHttpRequest();
                    ajax.open("POST", '/draw', false);
                    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    ajax.send(data);
                    leftField.innerHTML = "<img src=" + ajax.response + "?" + Date.now() + ">";
                    break;
                case 'triangle':
                    x1 = document.querySelector("#x1").value;
                    y1 = document.querySelector("#y1").value;
                    x2 = document.querySelector("#x2").value;
                    y2 = document.querySelector("#y2").value;
                    x3 = document.querySelector("#x3").value;
                    y3 = document.querySelector("#y3").value;
                    color = document.querySelector("#color").value;
                    data = "type-of-figure=triangle&x1=" + x1 + "&y1=" + y1 + "&x2=" + x2 + "&y2=" + y2
                            + "&x3=" + x3 + "&y3=" + y3 + "&color=" + color;
                    ajax = new XMLHttpRequest();
                    ajax.open("POST", '/draw', false);
                    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    ajax.send(data);
                    leftField.innerHTML = "<img src=" + ajax.response + "?" + Date.now() + ">";
                    break;
                case 'text':
                    x1 = document.querySelector("#x1").value;
                    y1 = document.querySelector("#y1").value;
                    fontSize = document.querySelector("#font-size").value;
                    textAngle = document.querySelector("#text-angle").value;
                    text = document.querySelector("#mytextarea").value;
                    color = document.querySelector("#color").value;
                    data = "type-of-figure=text&x1=" + x1 + "&y1=" + y1 + "&font-size=" + fontSize + "&text-angle="
                            + textAngle + "&text=" + text + "&color=" + color;
                    ajax = new XMLHttpRequest();
                    ajax.open("POST", '/draw', false);
                    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    ajax.send(data);
                    console.dir(ajax.response);
                    leftField.innerHTML = "<img src=" + ajax.response + "?" + Date.now() + ">";
            }
        }
    }

if (tableWithSketches) {
    tableWithSketches.onclick = function (event)
    {
        let target = event.target.closest("tr");
        let authorImage = target.dataset.pathtosketch.slice(18, -4);
        let authorEmail = target.children[2].innerText;
        location.href="/?email=" + authorEmail + "&img=" + authorImage;
    }
}


